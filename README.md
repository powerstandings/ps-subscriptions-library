PS Subscriptions Library
------------------------

The subscriptions library provides an interface to any WAMPv2 router implementation
to get details about current topics with active subscribers.

PowerStandings uses this functionality in order to only send socket messages that have
a current listener.

##Installation

Right now the ISDC Git repository runs on port :7999. This breaks vendoring for
cross platform languages. So, for now we have to work around this by using a 3rd
party Git hosting provider, with security and access control. I have chosen 
BitBucket because it provides all of these things for free, it is basically just
Atlassian's hosted Stash platform, and because I already had an account.

For the time being, in order to work on, or install PS libraries, you will need
your own Bitbucket account. Be sure to add your public key to your BitBucket account.
Once you have an account send Camron (clevanger@insidesales.com) your Bitbucket 
username and email address so that you can be given access to the repositories.

Alternatively, if you do not need write access, or if the access is for a build
system/other automation, just send Camron the public key and access can be granted
that way.

Once you have an account, and have sent your info to Camron, you need to configure
Git to use the proper URL's for BitBucket, otherwise you will constantly be prompted
for credentials.

Add the following lines to `~/.gitconfig`:

```
[url "git@bitbucket.org:"]
     insteadOf = https://bitbucket.org/
```

Once you have access:

`go get bitbucket.org/noisewaterphd/ps-subscriptions-library.git`

Then to save the version of dependency to your project:

`godep save`

##Usage

Usage example:

```
import "bitbucket.org/noisewaterphd/ps-subscriptions-library.git"

sub := subscriptions.GetSubscriptions()

conf := sub.SubscriptionsConfig{}

wamp := sub.WampConnection{}
wamp.Realm = "realm1"
wamp.Url = "ws://eng.test01.lan:8081/ws"

conf.WampConnections = append(conf.WampConnections, wamp)

err := sub.Initialize(conf, 30)

...


has_subscriber, err := sub.HasSubscriber("my.topic.to.check")

if has_subscriber {
	...send socket message...
}
```
