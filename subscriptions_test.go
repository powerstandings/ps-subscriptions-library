package subscriptions

import "testing"
import "time"

func TestInitializeSubscriptionManager(t *testing.T) {

	conf := SubscriptionsConfig{}
	wamp := WampConnection{}

	wamp.Realm = "realm1"
	wamp.Url = "ws://eng.test01.lan:8081/ws"

	conf.WampConnections = append(conf.WampConnections, wamp)

	sub := GetSubscriptions()

	err := sub.Initialize(conf, 30)

	time.Sleep(time.Second * 15)

	if err != nil {
		t.Error("This should be impossible, you really screwed up")
	}

	has, err := sub.HasSubscriber("notifications.6")
	if err != nil {
		t.Error("There was an error while checking subscribers: ", err.Error())
	}

	if err != nil && !has {
		t.Error("There was an error while checking subscribers, AND we got a false return: ", err.Error())
	}

}
