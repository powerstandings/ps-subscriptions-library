package subscriptions

import (
	"log"
	"time"

	"github.com/patrickmn/go-cache"
	"gopkg.in/jcelliott/turnpike.v2"
)

var Cache *cache.Cache
var CacheTTL time.Duration
var Config SubscriptionsConfig

type ISubscriptions interface {
	GetActiveSubscriptionList() (map[string][]Subscription, error)
	GetSubscription(topic string) (Subscription, error)
	HasSubscriber(topic string) (bool, error)
	Initialize(conf SubscriptionsConfig, ttl int) error
	MakeInitialCalls() error
	AddSubscription(sub Subscription)
	GetSubscriptionDetailsFromSessionID(client *turnpike.Client, id interface{}) (Subscription, error)
}

type WampConnection struct {
	Url   string
	Realm string
}

type SubscriptionsConfig struct {
	WampConnections []WampConnection
	Debug           bool
}

type Subscription struct {
	Uri     string
	Id      interface{}
	Created time.Time
	Active  bool
	Node    string
}

type Subscriptions struct {
	ISubscriptions
}

func GetSubscriptions() *Subscriptions {
	var subscriptions = Subscriptions{}
	return &subscriptions
}

// GetActiveSubscriptionList returns only from the WAMP routers all active subscriptions.
func (s *Subscriptions) GetActiveSubscriptionList() (map[string][]Subscription, error) {
	subscriptions := make(map[string][]Subscription)
	var node string

	for _, crossbar := range Config.WampConnections {

		client, err := turnpike.NewWebsocketClient(turnpike.JSON, crossbar.Url)
		if err != nil {
			continue
		}

		node = crossbar.Url

		_, err = client.JoinRealm(crossbar.Realm, nil)
		if err != nil {
			client.Close()
			continue
		}

		result, err := client.Call("wamp.subscription.list", []interface{}{}, make(map[string]interface{}))
		if err != nil {
			client.Close()
			continue
		}

		sessions := result.Arguments[0].(map[string]interface{})["exact"].([]interface{})

		for _, id := range sessions {
			subscription, err := s.GetSubscriptionDetailsFromSessionID(client, id)
			if err != nil {
				continue
			}

			subscription.Node = node
			subscription.Active = true
			subscriptions[node] = append(subscriptions[node], subscription)
		}

		client.Close()
	}

	return subscriptions, nil
}

// GetSubscription returns first from cache, then from the WAMP router subscription
// information matching the topic given as the topic parameter.
func (s *Subscriptions) GetSubscription(topic string) (Subscription, error) {
	var subscription Subscription

	if sub, found := Cache.Get(topic); found {
		subscription = sub.(Subscription)
		return subscription, nil
	} else {
		for _, crossbar := range Config.WampConnections {

			client, err := turnpike.NewWebsocketClient(turnpike.JSON, crossbar.Url)
			if err != nil {
				return subscription, err
			}

			_, err = client.JoinRealm(crossbar.Realm, nil)
			if err != nil {
				client.Close()
				return subscription, err
			}

			wamp_result, err := client.Call("wamp.subscription.match", []interface{}{topic}, make(map[string]interface{}))
			if err != nil {
				client.Close()
				return subscription, err
			}

			id := wamp_result.Arguments[0]

			if id != nil {
				subscription, err := s.GetSubscriptionDetailsFromSessionID(client, id.(float64))
				if err != nil {
					client.Close()
					return subscription, err
				}
				subscription.Active = true
				client.Close()
				break
			} else {
				subscription.Active = false
			}
			subscription.Node = crossbar.Url
			client.Close()
		}
	}

	s.AddSubscription(subscription)
	return subscription, nil
}

// HasSubscriber first checks internal memory cache, then makes an RPC call to each
// configured Crossbar.io router to find out if there is an exisitng WAMP subscriber
// for the topic provided as a parameter.
func (s *Subscriptions) HasSubscriber(topic string) (bool, error) {
	var result = false
	var subscription Subscription

	if subfound, found := Cache.Get(topic); found {
		subscription = subfound.(Subscription)
		return subscription.Active, nil
	} else {
		for _, crossbar := range Config.WampConnections {

			if result {
				return true, nil
			}

			client, err := turnpike.NewWebsocketClient(turnpike.JSON, crossbar.Url)
			if err != nil {
				return true, err
			}

			_, err = client.JoinRealm(crossbar.Realm, nil)
			if err != nil {
				client.Close()
				return true, err
			}

			wamp_result, err := client.Call("wamp.subscription.match", []interface{}{topic}, make(map[string]interface{}))
			if err != nil {
				client.Close()
				log.Fatal(err.Error())
			}

			id := wamp_result.Arguments[0]
			var real_id float64

			switch typed := id.(type) {
			case float64:
				real_id = typed
			case []interface{}:
				real_id = typed[0].(float64)
			}

			if id != nil {
				subscription, err = s.GetSubscriptionDetailsFromSessionID(client, real_id)
				if err != nil {
					client.Close()
					return true, err
				}
				subscription.Active = true
				s.AddSubscription(subscription)
				result = true
			}
			client.Close()
		}
		subscription.Uri = topic
		subscription.Active = false
		s.AddSubscription(subscription)
	}

	return false, nil
}

// Initialize sets global variables for WAMP connections, TTL, and makes an
// initial call to all Crossbar.io instances configured in order to get a current
// list of WAMP subscriptions to populate the internal memory cache.
func (s *Subscriptions) Initialize(conf SubscriptionsConfig, ttl int) error {

	Cache = cache.New(
		time.Duration(9999)*time.Hour,
		time.Duration(30)*time.Second,
	)

	CacheTTL = time.Duration(ttl) * time.Second

	if conf.Debug {
		turnpike.Debug()
	}

	Config = conf

	err := s.MakeInitialCalls()

	return err
}

// MakeInitialCalls performs an RPC to each configured WAMP router to retrieve
// a list of current subscriptions. It then adds the subscriptions to the Cache.
func (s *Subscriptions) MakeInitialCalls() error {

	for _, crossbar := range Config.WampConnections {

		client, err := turnpike.NewWebsocketClient(turnpike.JSON, crossbar.Url)
		if err != nil {
			return err
		}

		_, err = client.JoinRealm(crossbar.Realm, nil)
		if err != nil {
			return err
		}

		result, err := client.Call("wamp.subscription.list", []interface{}{}, make(map[string]interface{}))
		if err != nil {
			return err
		}

		sessions := result.Arguments[0].(map[string]interface{})["exact"].([]interface{})

		for _, id := range sessions {
			subscription, err := s.GetSubscriptionDetailsFromSessionID(client, id)
			if err != nil {
				continue
			}

			subscription.Active = true
			s.AddSubscription(subscription)
		}

		client.Close()
	}
	return nil
}

// AddActiveSubscription adds the given subscription struct to the memory cache.
func (s *Subscriptions) AddSubscription(sub Subscription) {

	if result, found := Cache.Get(sub.Uri); found {
		_ = result.(Subscription)
	} else {
		Cache.Set(
			sub.Uri,
			sub,
			time.Duration(CacheTTL)*time.Second,
		)
	}
}

// GetSubscriptionDetailsFromSessionID makes an RPC call to the WAMP router to
// get full subscription details for the given subscription id.
func (s *Subscriptions) GetSubscriptionDetailsFromSessionID(client *turnpike.Client, id interface{}) (Subscription, error) {
	subscription := Subscription{}
	result, err := client.Call("wamp.subscription.get", []interface{}{id}, make(map[string]interface{}))
	if err != nil {
		return subscription, nil
	}

	uri := result.Arguments[0].(map[string]interface{})["uri"].(string)
	sub_id := result.Arguments[0].(map[string]interface{})["id"]
	created := result.Arguments[0].(map[string]interface{})["created"].(string)

	layout := "2006-01-02T15:04:05.000Z"
	t, err := time.Parse(layout, created)

	if err != nil {
		log.Println(err)
	}

	subscription.Uri = uri
	subscription.Id = sub_id
	subscription.Created = t

	return subscription, nil
}
